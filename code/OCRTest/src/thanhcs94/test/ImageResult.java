package thanhcs94.test;


import java.io.File;

import com.googlecode.tesseract.android.TessBaseAPI;

import edu.sfsu.cs.orange.ocr.CaptureActivity;
import edu.sfsu.cs.orange.ocr.OcrResult;
import edu.sfsu.cs.orange.ocr.PreferencesActivity;
import edu.sfsu.cs.orange.ocr.R;
import edu.sfsu.cs.orange.ocr.ViewfinderView;
import edu.sfsu.cs.orange.ocr.language.TranslateAsyncTask;
import edu.sfsu.cs.orange.ocr.language.TranslateAsyncTaskResult;
import edu.sfsu.cs.orange.ocr.language.TranslateYandexAsyncTask;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageResult extends Activity{

	String TAG = getClass().getName();
	ImageView imgView;
	TextView tv, tvTranslate;
	private String sourceLanguageCode="en";
	private String targetLanguageCode="vi";
	 String textResult="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(PreferencesActivity.LANGUAGE_SOURCE.equalsIgnoreCase("eng")){
			sourceLanguageCode="en";
		    targetLanguageCode="vi";
			Log.wtf(TAG,"eng : " +sourceLanguageCode +" -> "+ targetLanguageCode);
		}else{
			sourceLanguageCode="vi";
		    targetLanguageCode="en";
			Log.wtf(TAG,"!eng : " +sourceLanguageCode +" -> "+ targetLanguageCode);
		}
		String tvFinal  = CaptureActivity.textResult;
		 // Get the translation asynchronously
	      new TranslateAsyncTaskResult(this, sourceLanguageCode, targetLanguageCode, 
	    		  tvFinal).execute();
		setContentView(R.layout.imageresult);
		imgView = (ImageView)findViewById(R.id.imageView1);
		tv = (TextView)findViewById(R.id.textView2);
		tvTranslate = (TextView)findViewById(R.id.textView3);
		imgView.setImageBitmap(CaptureActivity.bm);
	
		tv.setText(CaptureActivity.textResult);
		new TranslateYandexAsyncTask(this, sourceLanguageCode, targetLanguageCode, CaptureActivity.textResult , tvTranslate).execute();
		findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bitmap b = toGrayscale(CaptureActivity.bm);
				imgView.setImageBitmap(b);
				
//				CaptureActivity.baseApi.setImage(b);
//		    	//Cap baseApi.setImage(CaptureActivity.bm);
//		    	//@SuppressWarnings("unused")
//				//String recognizedText = CaptureActivity.baseApi.getUTF8Text();
//		    	CaptureActivity.baseApi.end();
//				String textResult = CaptureActivity.baseApi.getUTF8Text();
//				// int timeRequired = System.currentTimeMillis() - 0;
//				 // Check for failure to recognize text
//				 if (textResult == null || textResult.equals("")) {
//				// return null;
//				 }
//				 OcrResult ocrResult = new OcrResult();
//				 ocrResult.setWordConfidences(CaptureActivity.baseApi.wordConfidences());
//				 ocrResult.setMeanConfidence( CaptureActivity.baseApi.meanConfidence());
//				 if (ViewfinderView.DRAW_REGION_BOXES) {
//				 ocrResult.setRegionBoundingBoxes(CaptureActivity.baseApi.getRegions().getBoxRects());
//				 }
//				if (ViewfinderView.DRAW_TEXTLINE_BOXES) {ocrResult.setTextlineBoundingBoxes(CaptureActivity.baseApi.getTextlines().getBoxRects());
//				 }
//				 if (ViewfinderView.DRAW_STRIP_BOXES) {
//				 ocrResult.setStripBoundingBoxes(CaptureActivity.baseApi.getStrips().getBoxRects());
//				 }
//				 imgView.setImageBitmap(ocrResult.getBitmap());
//				 tv.setText(textResult);
				 
			}
		});
		
		
		
//		baseApi = new TessBaseAPI();
//		if (baseApi != null) {
//		      baseApi.setPageSegMode(pageSegmentationMode);
//		      baseApi.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, characterBlacklist);
//		      baseApi.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, characterWhitelist);
//		    }
		//tv.setText(CaptureActivity.textResult);
		//TessBaseAPI baseApi = new TessBaseAPI();
    	// DATA_PATH = Path to the storage
    	// lang = for which the language data exists, usually "eng"
    	//baseApi.init(DATA_PATH, lang);
    	// Eg. 
    	//baseApi.init("/mnt/sdcard/tesseract/tessdata/grc.traineddata", "eng");
		
//			
		findViewById(R.id.imageView2).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
	//crop and grey scale Bitmap
	  public Bitmap toGrayscale(Bitmap bmpOriginal)
	  {        
	      int width, height;
	      height = bmpOriginal.getHeight();
	      width = bmpOriginal.getWidth();    

	      Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	      Canvas c = new Canvas(bmpGrayscale);
	      Paint paint = new Paint();
	      ColorMatrix cm = new ColorMatrix();
	      cm.setSaturation(0);
	      ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
	      paint.setColorFilter(f);
	      c.drawBitmap(bmpOriginal, 0, 0, paint);
	      return bmpGrayscale;
	  } 
	  
//	TessBaseAPI getBaseApi() {
//	    return baseApi;
//	  }
//	
//	/**
//	   * Gets values from shared preferences and sets the corresponding data members in this activity.
//	   */
//	  private void retrievePreferences() {
//		 SharedPreferences prefs;
//		  prefs = PreferenceManager.getDefaultSharedPreferences(this);
//	      // Retrieve from preferences, and set in this Activity, the character blacklist and whitelist
//	   //   characterBlacklist = OcrCharacterHelper.getBlacklist(prefs, CaptureActivity.sourceLanguageCodeOcr);
//	 //     characterWhitelist = OcrCharacterHelper.getWhitelist(prefs, CaptureActivity.sourceLanguageCodeOcr);
//	     
//	  }
}
