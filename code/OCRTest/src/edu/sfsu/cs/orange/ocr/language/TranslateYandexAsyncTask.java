package edu.sfsu.cs.orange.ocr.language;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.google.gson.JsonObject;

import thanhcs94.test.API;
import thanhcs94.test.ImageResult;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class TranslateYandexAsyncTask extends AsyncTask<String, String, Boolean> {
   private static final String TAG = TranslateYandexAsyncTask.class.getSimpleName();
   public static String YANDEXT_API_KEY = "trnsl.1.1.20151119T155320Z.2c3fadc3bed8c7ac.e6415da923a4da409da8d067ae2564344bc12bd5";
	public static String URL = "";
	private ImageResult activity;
	private String sourceLanguageCode;
	private String targetLanguageCode;
	private String sourceText;
	private String translatedText = "";
	private String textTranslate= "Error translate";
	private InputStream is=null;
	private JSONObject jobj=null;
	private String json;
	private TextView tvResult;
	public TranslateYandexAsyncTask(ImageResult activity, String sourceLanguageCode, String targetLanguageCode, 
			String sourceText, TextView tvResult) {
		this.activity = activity;
		this.sourceLanguageCode = sourceLanguageCode;
		this.targetLanguageCode = targetLanguageCode;
		this.sourceText = sourceText;
		this.tvResult = tvResult;
	}

	@Override
	protected Boolean doInBackground(String... arg0) {
		try{
			sourceText=sourceText.replace(" ", "%20");
			sourceText=sourceText.replace("\n", "%20");
			System.out.println(sourceText);
			URL= "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+
			YANDEXT_API_KEY+"&text="+sourceText+"&lang="+sourceLanguageCode+"-"+targetLanguageCode+"&format=plain";
			System.out.println(URL);
			JSONObject jsonObject = getJSONFromUrl(URL);
			textTranslate = jsonObject.getString("text");
			System.out.println(jsonObject);
			System.out.println(textTranslate);
		}catch(Exception e){
			System.out.println("error");
		}
		if (translatedText.equals(Translator.BAD_TRANSLATION_MSG)) {
			return false;
		}
		return true;
	}

	@Override
	protected synchronized void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		tvResult.setText(textTranslate);
	}
	
	public JSONObject getJSONFromUrl(String url)
	{
		try{
			//khoi tao
			DefaultHttpClient client=new DefaultHttpClient();
			HttpPost httppost=new HttpPost(url);
			HttpResponse httpresponse=client.execute(httppost);
			HttpEntity httpentity=httpresponse.getEntity();
			is=httpentity.getContent();
			BufferedReader reader=
					new BufferedReader(new InputStreamReader(is,"utf-8"),8);
			StringBuilder sb=new StringBuilder();
			String line=null;
			while((line=reader.readLine())!=null){
				sb.append(line+"\n");
			}
			is.close();
			json=sb.toString();
			jobj=new JSONObject(json);
		}catch(Exception e)
		{
			Log.d("error translate", e.toString());
		}
		return jobj;
	}

}